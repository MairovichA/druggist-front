export const sortBy = (array: any, fieldName: string) => {
  array.sort((a, b) => {
    if (a[fieldName] < b[fieldName]) {
      return 1;
    } else if (a[fieldName] > b[fieldName]) {
      return -1;
    } else {
      return 0;
    }
  });
};
