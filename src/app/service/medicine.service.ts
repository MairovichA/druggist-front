import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserService} from "./user.service";
import {Configuration} from "../configuration";
import {HttpHeaders} from '@angular/common/http';
import {Medicine} from "../model/medicine";
import {filter, map} from 'rxjs/operators';
import {sortBy} from '../helpers/sort';


@Injectable({
  providedIn: 'root'
})
export class MedicineService {
  public className = MedicineService.name;

  constructor(public http: HttpClient,
              public userService: UserService) {
  }

  public getAll(): Observable<Array<Medicine>> {
    console.log(`${this.className}.getAll()`);

    const httpOptions = this.createOptions();

    return this.http.get<Array<Medicine>>(`${Configuration.basePath}/user/${this.userService.id}/meds`, httpOptions)
      .pipe(
        map(medicines => {
          sortBy(medicines, 'id');
          return medicines;
        })
      );
  }

  public save(medicine: Medicine): Observable<Medicine> {
    console.log(`${this.className}.save(): medicine=${JSON.stringify(medicine)}`);

    const httpOptions = this.createOptions();
    return this.http.post<Medicine>(`${Configuration.basePath}/user/${this.userService.id}/meds`, medicine, httpOptions);
  }

  public getById(id: number): Observable<Medicine> {
    console.log(`${this.className}.getById(): id=${id}`);

    const httpOptions = this.createOptions();

    return this.http.get<Medicine>
    (`${Configuration.basePath}/user/${this.userService.id}/meds/${id}`, httpOptions);
  }

  update(medicine: Medicine) : Observable<any> {
    console.log(`${this.className}.update(): medicine=${JSON.stringify(medicine)}`);

    const httpOptions = this.createOptions();
    return this.http.put<Medicine>(`${Configuration.basePath}/user/${this.userService.id}/meds/${medicine.id}`, medicine, httpOptions);
  }

  delete(id: number) : Observable<any>{
    console.log(`${this.className}.delete(): id=${id}`);

    const httpOptions = this.createOptions();
    return this.http.delete<Medicine>(`${Configuration.basePath}/user/${this.userService.id}/meds/${id}`, httpOptions);
  }

  public createOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.userService.authHeader
      })
    };
  }
}
