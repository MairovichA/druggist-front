import {Injectable} from '@angular/core';
import {User} from '../model/user';
import '../configuration';

import {HttpClient} from '@angular/common/http';
import {Configuration} from "../configuration";
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public className = UserService.name;

  public _jwt: string;
  public _id: number;
  public _login: string;
  public _email: string;
  public _expiresAtInMs: number;
  public _redirectUrl: string;

  constructor(public http: HttpClient,
              public router: Router) {
  }

  public register(user: User) {
    console.log(`${this.className} Register user : ${JSON.stringify(user)}`);

    return this.http.post<AuthenticationResponse>(`${Configuration.basePath}/user/registration`, user);
  }

  public auth(email: string, password: string) {
    console.log(`${this.className} auth user : email=${email}, password=${password}`);

    let authRequest = {
      email: email,
      password: password
    };
    return this.http.post<AuthenticationResponse>(`${Configuration.basePath}/user/login`, authRequest);
  }

  public logout() {
    sessionStorage.clear();
    this._jwt = null;
    this._id = null;
    this._login = null;
    this._email = null;
    this._expiresAtInMs = null;
    this.router.navigate(['/auth']);
  }

  set jwt(jwt: string) {
    console.log(`${this.className}.jwt=${jwt}`);
    this._jwt = jwt;

    let decoded = atob(jwt.split('.')[1]);
    let data = JSON.parse(decoded);
    console.log(`jwt data=${JSON.stringify(decoded)}`);
    this._id = data.sub;
    this._login = data.login;
    this._email = data.email;
    this._expiresAtInMs = data.exp * 1000;
    sessionStorage.setItem('jwt', this._jwt);
    sessionStorage.setItem('login', this._login);
    sessionStorage.setItem('email', this._email);
    sessionStorage.setItem('id', data.sub);
    sessionStorage.setItem('expiresAtInMs', String(data.ext * 1000));
  }

  get jwt(): string {
    return sessionStorage.getItem('jwt');
  }

  get authHeader(): string{
    return 'Bearer ' + sessionStorage.getItem('jwt');
  }

  get login(): string {
    return sessionStorage.getItem('login');
  }

  get email(): string {
    return sessionStorage.getItem('email');
  }

  get id(): number {
    return Number(sessionStorage.getItem('id'));
  }

  get expiresAt(): number {
    return Number(sessionStorage.getItem('expiresAtInMs'));
  }

  get isAuthenticated() {
    console.log(`${this.className}.isAuthenticated`);
    return Date.now() < Number(sessionStorage.getItem('expiresAtInMs'));
  }

  set redirectUrl(url: string) {
    this._redirectUrl = url;
  }

}

export class AuthenticationResponse {
  public jwt: string;
}
