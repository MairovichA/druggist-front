import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from './user.service';
import {Observable} from 'rxjs';
import {Configuration} from '../configuration';
import {Category} from '../model/category';
import {map} from 'rxjs/operators';
import {sortBy} from '../helpers/sort';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  public className = CategoryService.name;

  constructor(public http: HttpClient,
              public userService: UserService) { }

  public getAll(): Observable<Array<Category>> {
    console.log(`${this.className}.getAll()`);

    const httpOptions = this.createOptions();

    return this.http.get<Array<Category>>(`${Configuration.basePath}/user/${this.userService.id}/category`, httpOptions)
      .pipe(
        map(categories => {
          sortBy(categories, 'id');
          return categories;
          }
        )
      );
  }

  public save(category: Category): Observable<Category> {
    console.log(`${this.className}.save(): category=${JSON.stringify(category)}`);

    const httpOptions = this.createOptions();
    return this.http.post<Category>(`${Configuration.basePath}/user/${this.userService.id}/category`, category, httpOptions);
  }

  public getById(id: number): Observable<Category> {
    console.log(`${this.className}.getById(): id=${id}`);

    const httpOptions = this.createOptions();

    return this.http.get<Category>
    (`${Configuration.basePath}/user/${this.userService.id}/category/${id}`, httpOptions);
  }

  update(category: Category): Observable<any> {
    console.log(`${this.className}.update(): category=${JSON.stringify(category)}`);

    const httpOptions = this.createOptions();
    return this.http.put<Category>(`${Configuration.basePath}/user/${this.userService.id}/category/${category.id}`, category, httpOptions);
  }

  delete(id: number) : Observable<any>{
    console.log(`${this.className}.delete(): id=${id}`);

    const httpOptions = this.createOptions();
    return this.http.delete<Category>(`${Configuration.basePath}/user/${this.userService.id}/category/${id}`, httpOptions);
  }

  public createOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.userService.authHeader
      })
    };
  }
}
