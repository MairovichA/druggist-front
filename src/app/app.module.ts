import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './component/register/register.component';
import { AuthComponent } from './component/auth/auth.component';
import { ImComponent } from './component/im/im.component';
import { MedicineListComponent } from './component/medicine/medicine-list/medicine-list.component';
import { AddMedicineComponent } from './component/medicine/add-medicine/add-medicine.component';
import { MedicineDetailComponent } from './component/medicine/medicine-detail/medicine-detail.component';
import { MedicineEditComponent } from './component/medicine/medicine-edit/medicine-edit.component';
import { CategoryListComponent } from './component/category/category-list/category-list.component';
import { CategoryAddComponent } from './component/category/category-add/category-add.component';
import { CategoryDetailComponent } from './component/category/category-detail/category-detail.component';
import { CategoryEditComponent } from './component/category/category-edit/category-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatInputModule,
  MatCardModule, MatDatepickerModule, MatNativeDateModule, MatExpansionModule
} from '@angular/material';
import { ExpiredDialogComponent } from './component/medicine/expired-dialog/expired-dialog.component';
import { SearchPipe } from './component/medicine/medicine-list/search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    AuthComponent,
    ImComponent,
    MedicineListComponent,
    AddMedicineComponent,
    MedicineDetailComponent,
    MedicineEditComponent,
    CategoryListComponent,
    CategoryAddComponent,
    CategoryDetailComponent,
    CategoryEditComponent,
    LayoutComponent,
    ExpiredDialogComponent,
    SearchPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule
  ],
  entryComponents: [
    ExpiredDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
