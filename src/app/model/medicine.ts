export class Medicine {
  public id: number;
  public name: string;
  public description: string;
  public expirationDate: string;
  public categories: Array<number>;
}
