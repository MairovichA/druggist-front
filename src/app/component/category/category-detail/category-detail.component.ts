import { Component, OnInit } from '@angular/core';
import {Medicine} from '../../../model/medicine';
import {MedicineService} from '../../../service/medicine.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CategoryService} from '../../../service/category.service';
import {Category} from '../../../model/category';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {

  public className = CategoryDetailComponent.name;

  category: Category;

  constructor(public categoryService: CategoryService,
              public route: ActivatedRoute,
              public router: Router) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.categoryService.getById(id).subscribe(category => {
      this.category = category;
    });
  }

  delete(category: Category) {
    console.log(`${this.className}.delete() : id=${category.id}`);

    this.categoryService.delete(category.id).subscribe(result => {
      this.router.navigate(['/categories-list']);
    });
  }

  edit(category: Category) {
    this.router.navigate(['/category', category.id, 'edit']);
  }

}
