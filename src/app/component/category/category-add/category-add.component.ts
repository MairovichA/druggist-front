import { Component, OnInit } from '@angular/core';
import {Medicine} from '../../../model/medicine';
import {MedicineService} from '../../../service/medicine.service';
import {Router} from '@angular/router';
import {Category} from '../../../model/category';
import {CategoryService} from '../../../service/category.service';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {

  public className = CategoryAddComponent.name;

  public category: Category;

  constructor(public categoryService: CategoryService,
              public router: Router) {
  }

  ngOnInit() {
    this.category = new Category();
  }

  save(category: Category) {
    console.log(`${this.className}.save() : ${JSON.stringify(category)}`);

    this.categoryService.save(category).subscribe(savedCategory => {
      console.log(`${this.className} successfully saved : ${JSON.stringify(savedCategory)}`);
      this.router.navigate(['/category', `${savedCategory.id}`]);
    });
  }

}
