import { Component, OnInit } from '@angular/core';
import {Medicine} from '../../../model/medicine';
import {MedicineService} from '../../../service/medicine.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Category} from '../../../model/category';
import {CategoryService} from '../../../service/category.service';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {

  public className = CategoryEditComponent.name;

  public category: Category;
  public editedCategory: Category;

  constructor(public categoryService: CategoryService,
              public route: ActivatedRoute,
              public router: Router) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.categoryService.getById(id).subscribe(category => {
      this.category = category;
      // this.initEditedMedicine();
    });
  }

  save(category) {
    console.log(`${this.className}.save() : id=${category .id}`);

    this.categoryService.update(category).subscribe(result =>{
      this.router.navigate(['/category', category.id]);
    })
  }

  /*private initEditedMedicine() {
    this.editedMedicine = new Medicine();
    this.editedMedicine.id = this.medicine.id;
    this.editedMedicine.name = this.medicine.name;
  }*/

}
