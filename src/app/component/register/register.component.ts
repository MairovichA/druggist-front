import {Component, OnInit} from '@angular/core';
import {UserService} from "../../service/user.service";
import {User} from "../../model/user";
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  login: string ;
  password: string;
  repeatedPassword: string;

  constructor(public userService: UserService,
              public router: Router) {
  }

  ngOnInit() {
  }

  register() {
    console.log(`${RegisterComponent.name}.register()`);

    const user = new User(this.login, this.email, this.password);

    this.userService.register(user).subscribe(response => {
      console.log(`${RegisterComponent.name}.register(): receive jwt ${JSON.stringify(response)}`);
      this.userService.jwt = response.jwt;
      this.router.navigate(['/im']);
    }, error => {
      // TODO handle error
      console.log(JSON.stringify(error));
    });
  }
}
