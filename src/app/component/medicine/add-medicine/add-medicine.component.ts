import {Component, OnInit, ViewChild} from '@angular/core';
import {Medicine} from "../../../model/medicine";
import {MedicineService} from "../../../service/medicine.service";
import {Router} from "@angular/router";
import {CategoryService} from '../../../service/category.service';
import {Category} from '../../../model/category';

@Component({
  selector: 'app-add-medicine',
  templateUrl: './add-medicine.component.html',
  styleUrls: ['./add-medicine.component.css']
})
export class AddMedicineComponent implements OnInit {
  @ViewChild('selection') selection;
  public className = AddMedicineComponent.name;

  public medicine: Medicine;
  public categories: Array<Category> = [];
  public selectedCategories;

  constructor(public medicineService: MedicineService,
              public categoryService: CategoryService,
              public router: Router) {
  }

  ngOnInit() {
    this.medicine = new Medicine();
    this.categoryService.getAll().subscribe(categories => {
      this.categories = categories;
      console.log(this.categories);
    });
  }

  save(medicine: Medicine) {
    console.log(`${this.className}.save() : ${JSON.stringify(medicine)}`);

    this.medicineService.save(medicine).subscribe(savedMedicine =>{
      console.log(`${this.className} successfully saved : ${JSON.stringify(savedMedicine)}`);
      this.router.navigate(['/medicine', `${savedMedicine.id}`]);
    })
  }
}
