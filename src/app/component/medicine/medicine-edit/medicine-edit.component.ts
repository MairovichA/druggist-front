import {Component, OnInit, ViewChild} from '@angular/core';
import {Medicine} from "../../../model/medicine";
import {MedicineService} from "../../../service/medicine.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from '../../../service/category.service';

@Component({
  selector: 'app-medicine-edit',
  templateUrl: './medicine-edit.component.html',
  styleUrls: ['./medicine-edit.component.css']
})
export class MedicineEditComponent implements OnInit {
  @ViewChild('selection') selection;
  public className = MedicineEditComponent.name;

  public medicine: Medicine;
  public editedMedicine: Medicine;
  public categories;

  constructor(public medicineService: MedicineService,
              public categoryService: CategoryService,
              public route: ActivatedRoute,
              public router: Router) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.medicineService.getById(id).subscribe(medicine => {
      this.medicine = medicine;
      console.log(this.medicine.categories);
      this.categoryService.getAll().subscribe(categories => {
        this.categories = categories;
        this.medicine.categories = this.medicine.categories.map(item => item['id']);
      });
    });
  }

  save(medicine: Medicine) {
    console.log(`${this.className}.save() : id=${medicine.id}`);

    this.medicineService.update(medicine).subscribe(result => {
      this.router.navigate(['/medicine', medicine.id]);
    });
  }
}
