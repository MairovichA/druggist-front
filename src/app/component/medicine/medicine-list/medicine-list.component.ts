import {Component, OnInit} from '@angular/core';
import {MedicineService} from "../../../service/medicine.service";
import {Medicine} from "../../../model/medicine";
import {MatDialog} from '@angular/material';
import {ExpiredDialogComponent} from '../expired-dialog/expired-dialog.component';

@Component({
  selector: 'app-medicine-list',
  templateUrl: './medicine-list.component.html',
  styleUrls: ['./medicine-list.component.scss']
})
export class MedicineListComponent implements OnInit {
  medicines: Array<Medicine> = [];
  name: string = '';
  description: string = '';
  data = '';

  constructor(public medicineService: MedicineService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.medicineService.getAll().subscribe(medicines => {
      this.medicines = medicines;
      const currentDate = new Date();
      const expiredMedicines = [];
      this.medicines.forEach(medicine => {
        if (currentDate > new Date(medicine.expirationDate)) {
          expiredMedicines.push(medicine);
        }
      });
      if (expiredMedicines.length > 0) {
        const dialogRef = this.dialog.open(ExpiredDialogComponent, {
          width: '500px',
          data: {expiredMedicines}
        });
      }

    });
  }

  public isExpired(medicine: Medicine): boolean {
    const currentDate = new Date();
    return currentDate > new Date(medicine.expirationDate);
  }
}
