import {Pipe, PipeTransform} from '@angular/core';
import {Medicine} from "../../../model/medicine";

@Pipe({
  name: 'search',
  pure: false
})
export class SearchPipe implements PipeTransform {

  transform(medicines: Array<Medicine>, data: string): any {
    // tslint:disable-next-line:max-line-length
    const filterByData = medicine => data === '' || (data !== '' && medicine.name.includes(data)) || (data !== '' && medicine.description !== null && medicine.description.includes(data));

    return medicines.filter(filterByData);
  }

}
