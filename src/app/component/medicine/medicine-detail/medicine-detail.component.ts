import {Component, OnInit} from '@angular/core';
import {Medicine} from "../../../model/medicine";
import {MedicineService} from "../../../service/medicine.service";
import {ActivatedRoute, Router} from '@angular/router';
@Component({
  selector: 'app-medicine-detail',
  templateUrl: './medicine-detail.component.html',
  styleUrls: ['./medicine-detail.component.css']
})
export class MedicineDetailComponent implements OnInit {
  public className = MedicineDetailComponent.name;
  public isMedicineValid = true;

  medicine: Medicine;

  constructor(public medicineService: MedicineService,
              public route: ActivatedRoute,
              public router: Router) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.medicineService.getById(id).subscribe(medicine => {
      this.medicine = medicine;
      if (medicine.expirationDate) {
        const currentDate = new Date();
        if (currentDate > new Date(medicine.expirationDate)) {
          this.isMedicineValid = false;
        }
      }
    });
  }

  delete(medicine: Medicine) {
    console.log(`${this.className}.delete() : id=${medicine.id}`);

    this.medicineService.delete(medicine.id).subscribe(result => {
      this.router.navigate(['/medicine-list']);
    });
  }

  edit(medicine: Medicine) {
    this.router.navigate(['/medicine', medicine.id, 'edit']);
  }
}
