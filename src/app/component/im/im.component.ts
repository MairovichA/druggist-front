import {Component, OnInit} from '@angular/core';
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-im',
  templateUrl: './im.component.html',
  styleUrls: ['./im.component.css']
})
export class ImComponent implements OnInit {

  constructor(public userService: UserService) {
  }

  ngOnInit() {
  }
}
