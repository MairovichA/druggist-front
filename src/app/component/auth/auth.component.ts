import { Component } from '@angular/core';
import {UserService} from "../../service/user.service";
import {Router} from "@angular/router";
import {User} from "../../model/user";


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {
  email: string;
  password: string;

  constructor(public userService: UserService,
              public router: Router) {
  }

  authenticate() {
    console.log(`${AuthComponent.name}.authenticate()`);

    this.userService.auth(this.email, this.password).subscribe(response => {
      console.log(`${AuthComponent.name}.authenticate(): receive jwt ${JSON.stringify(response)}`);
      this.userService.jwt = response.jwt;
      sessionStorage.setItem('jwt', response.jwt);
      this.router.navigate(['/im']);
    }, error => {
      // TODO handle error
      console.log(JSON.stringify(error));
    });
  }

}
