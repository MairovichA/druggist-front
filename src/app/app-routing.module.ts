import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RegisterComponent} from './component/register/register.component';
import {AuthComponent} from './component/auth/auth.component';
import {ImComponent} from "./component/im/im.component";
import {AuthGuard} from "./auth/auth.guard";
import {MedicineListComponent} from "./component/medicine/medicine-list/medicine-list.component";
import {AddMedicineComponent} from "./component/medicine/add-medicine/add-medicine.component";
import {MedicineDetailComponent} from "./component/medicine/medicine-detail/medicine-detail.component";
import {MedicineEditComponent} from "./component/medicine/medicine-edit/medicine-edit.component";
import {CategoryListComponent} from './component/category/category-list/category-list.component';
import {CategoryAddComponent} from './component/category/category-add/category-add.component';
import {CategoryDetailComponent} from './component/category/category-detail/category-detail.component';
import {CategoryEditComponent} from './component/category/category-edit/category-edit.component';
import {LayoutComponent} from './layout/layout.component';

const routes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: 'auth', component: AuthComponent},
  {path: 'main', component: LayoutComponent},
  {path: 'im', component: ImComponent, canActivate: [AuthGuard]},
  {path: 'medicine-list', component: MedicineListComponent, canActivate: [AuthGuard]},
  {path: 'add-medicine', component: AddMedicineComponent, canActivate: [AuthGuard]},
  {path: 'medicine/:id', component: MedicineDetailComponent, canActivate: [AuthGuard]},
  {path: 'medicine/:id/edit', component: MedicineEditComponent, canActivate: [AuthGuard]},
  {path: 'categories-list', component: CategoryListComponent, canActivate: [AuthGuard]},
  {path: 'category-add', component: CategoryAddComponent, canActivate: [AuthGuard]},
  {path: 'category/:id', component: CategoryDetailComponent, canActivate: [AuthGuard]},
  {path: 'category/:id/edit', component: CategoryEditComponent, canActivate: [AuthGuard]},
  {path: '', redirectTo: 'im', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
